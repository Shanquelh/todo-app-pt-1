import React, { Component } from "react";
import todosList from "./todos.json";

class App extends Component {
  state = {
    todos: todosList,
  };

  handleDeleteEach = (event, todoid) => {
    const handlingDelete = this.state.todos.filter(todo => { 
        if (todo.id !== todoid) {
         return true;
        }
        return false;
      });
    this.setState({ todos: handlingDelete });
  };

  clearAll = () => {
    const deleteIt = this.state.todos.filter(todo => {
      if (todo.completed === true ) {
        return false;
      }
      return true;
    });
    this.setState({ todos: deleteIt });
  };

  handleToggler = (event, toggledToDos) => {
    const newToDoListOfMenu = this.state.todos.map((todo) => {
      if (todo.id === toggledToDos) {
        return { ...todo, completed: !todo.completed };
      }
      return todo;
    });
    this.setState({ todos: newToDoListOfMenu });
  };

  handlerNewToDo = (event) => {
    if (event.key === "Enter" && "Return") {
      const newToDoList = {
        userId: 1,
        id: Math.floor(Math.random() * 100) + 1,
        title: event.target.value,
        completed: false,
      };
      const newToDoListMenu = this.state.todos;
      newToDoListMenu.push(newToDoList);
      this.setState({ todos: newToDoListMenu });
      event.target.value = "";
    }
  };

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            onKeyDown={this.handlerNewToDo}
            autoFocus
          />
        </header>
        <TodoList todos={this.state.todos} 
        handleToggler={this.handleToggler} 
        handleDeleteEach={this.handleDeleteEach} 
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button onClick={this.clearAll} className="clear-completed">Clear completed</button>
          
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            checked={this.props.completed}
            onChange={(event) => this.props.handleToggler(event, this.props.id)}
          />
          <label>{this.props.title}</label>
          <button
            className="destroy"
            onClick={this.props.handleDeleteEach}
          />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem
              title={todo.title}
              completed={todo.completed}
              id={todo.id}
              handleToggler={this.props.handleToggler}
              handleDeleteEach={(event) => this.props.handleDeleteEach(event, todo.id)}
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
